package api

import (
	"encoding/json"
	"example/domain"
	"example/domain/services"
	"net/http"

	"github.com/google/uuid"
)

type careRequestApi struct {
	service services.CareRequestService
}

func NewCareRequestApi(service services.CareRequestService) ServerInterface {
	return &careRequestApi{
		service: service,
	}
}

// Get all requests
// (GET /requests/{request-id})
func (c *careRequestApi) GetRequest(w http.ResponseWriter, r *http.Request, requestId RequestId, params GetRequestParams) {
	found, err := c.service.GetRequestById(domain.RequestId(requestId))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if found == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	response := CareRequest{
		Id:     uuid.UUID(found.Id),
		Status: RequestStatus(found.Status),
	}
	bytes, err := json.Marshal(response)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("content-type", "application/json")
	w.Write(bytes) // NOTE that we should handle the error here
	w.WriteHeader(http.StatusOK)
}
