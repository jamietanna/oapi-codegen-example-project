package api

import (
	"example/domain"
	"example/mocks"
	"fmt"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	validator "openapi.tanna.dev/go/validator/openapi3"
)

func TestCareRequestApi_GetRequest(t *testing.T) {
	mockService := mocks.NewMockCareRequestService(gomock.NewController(t))
	server := NewCareRequestApi(mockService)

	t.Run("when successful", func(t *testing.T) {
		found := domain.CareRequest{
			Id:     domain.RequestId(uuid.New()),
			Status: domain.RequestStatusActive,
		}

		mockService.EXPECT().GetRequestById(gomock.Any()).Return(&found, nil)

		requestId := uuid.New()
		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/requests/"+requestId.String(), nil)

		server.GetRequest(rr, req, requestId, GetRequestParams{})

		t.Run("it returns 200", func(t *testing.T) {
			assert.Equal(t, 200, rr.Result().StatusCode)
		})

		t.Run("it matches OpenAPI", func(t *testing.T) {
			doc, err := GetSwagger()
			assert.NoError(t, err)

			_ = validator.NewValidator(doc).ForTest(t, rr, req)
			// validation happens in the background
		})
	})

	t.Run("when no request found", func(t *testing.T) {
		mockService.EXPECT().GetRequestById(gomock.Any()).Return(nil, nil)

		requestId := uuid.New()
		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/requests/"+requestId.String(), nil)

		server.GetRequest(rr, req, requestId, GetRequestParams{})

		t.Run("it returns 404", func(t *testing.T) {
			assert.Equal(t, 404, rr.Result().StatusCode)
		})

		t.Run("it matches OpenAPI", func(t *testing.T) {
			doc, err := GetSwagger()
			assert.NoError(t, err)

			_ = validator.NewValidator(doc).ForTest(t, rr, req)
			// validation happens in the background
		})
	})

	t.Run("when an error returned from service", func(t *testing.T) {
		mockService.EXPECT().GetRequestById(gomock.Any()).Return(nil, fmt.Errorf("uh oh"))

		requestId := uuid.New()
		rr := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/requests/"+requestId.String(), nil)

		server.GetRequest(rr, req, requestId, GetRequestParams{})

		t.Run("it returns 500", func(t *testing.T) {
			assert.Equal(t, 500, rr.Result().StatusCode)
		})
	})
}
