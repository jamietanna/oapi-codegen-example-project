package server

import (
	"fmt"
	"net/http"
	"os"

	"example/domain/services"
	"example/server/api"

	middleware "github.com/deepmap/oapi-codegen/pkg/chi-middleware"
	"github.com/gorilla/mux"
)

// NewHandler prepares a new http.Handler that can then get wired into an http.Server
// This code is mostly taken from https://github.com/deepmap/oapi-codegen/blob/master/examples/petstore-expanded/gorilla/petstore.go
func NewHandler() http.Handler {
	openapi, err := api.GetSwagger()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error loading swagger spec\n: %s", err)
		os.Exit(1)
	}

	openapi.Servers = nil

	var service services.CareRequestService // TODO: in a production app, this would be properly initialised

	server := api.NewCareRequestApi(service)

	r := mux.NewRouter()

	// enforce consumers use the right contract, required on top of checks that are done in the generated API code
	r.Use(middleware.OapiRequestValidator(openapi))

	return api.HandlerFromMux(server, r)
}
