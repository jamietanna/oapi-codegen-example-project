package services

//go:generate go run github.com/golang/mock/mockgen -destination ../../mocks/carerequestservice.go -package mocks . CareRequestService

import "example/domain"

type CareRequestService interface {
	GetRequestById(id domain.RequestId) (*domain.CareRequest, error)
}
