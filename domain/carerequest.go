package domain

import "github.com/google/uuid"

type RequestId uuid.UUID

type CareRequest struct {
	Id     RequestId
	Status RequestStatus
}

type RequestStatus string

const (
	RequestStatusActive    = "active"
	RequestStatusCompleted = "completed"
)
